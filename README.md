# Ghidra Container

[![pipeline status](https://gitlab.com/Torgo/ghidra-container/badges/master/pipeline.svg)](https://gitlab.com/Torgo/ghidra-container/-/commits/master)

This repo contains a few Ghidra containers:

- A standrad Ghidra install
- A container for compiling Ghidra extensions

They can be found under the Container Registry menu here on GitLab.
